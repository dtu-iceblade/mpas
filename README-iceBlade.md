# MPAS-iceBlade
The iceBlade model is a diagnostic module extension for the Model for Prediction Across Scales (MPAS) that models the turbine airfoil as a static cylinder, and accounts for both ice accretion and ice ablation. 

This repository is https://gitlab.windenergy.dtu.dk/dtu-iceblade/mpas master branch.

## Branches
* master - the MPAS model with iceBlade implemented.
* NCAR - tracking the official MPAS repository including release tags

## Documentation and how to use
The documenation with detailed information on the installation, setup and verification test cases is available at https://dtu-iceblade.pages.windenergy.dtu.dk/docs/


## Developers
MPAS-iceBlade was developed by [DTU Wind](https://wind.dtu.dk/). 

## License
MPAS-iceBlade is available under the [MPAS public domain notice and disclaimer](https://gitlab.windenergy.dtu.dk/dtu-iceblade/mpas/-/blob/master/LICENSE). DTU Wind is to be credited when iceBlade is used, distributed, adapted, build upon or otherwise applied.

## Citation

MPAS-iceBlade, DTU Wind, https://gitlab.windenergy.dtu.dk/dtu-iceblade/mpas, [CHECKOUT-DATE], Check-out: [COMMIT-HASH]

Davis, N. N., Pinson, P., Hahmann, A. N., Clausen, N. -E., and Žagar, M. (2016) Identifying and characterizing the impact of turbine icing on wind farm power generation. Wind Energ., 19: 1503– 1518. https://doi.org/10.1002/we.1933 